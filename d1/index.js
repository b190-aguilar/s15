
	 	// use ctrl + / to comment out line
	 	/* 
	 	use ctrl + shift + / to comment out multiple lines
	 	*/
	 	// alert("Hello World");
	 	// console.log allows the browser to display the message that is inside the parenthesis in the Console section for dev tools.
 		console.log("Hello World");
 		
 		console.
 		log
 		(
 			"Hello Again"
 		);


	// Variables
	// it is uese to store data 
	// any information that is used by an application is stored in what we called "memory";
	// when we create variables, certain portions of a device's memory is given a "name" that we call "variables"

	// if the variable is called without declaration, the console would render that the variable we are calling is "not defined" 

	// variables are initialized with the use of "let/const" keyword;
	// after declaring the variable (declaring a variable means that we have created a variable and is ready to receive date), failure to assign a value would mean that the variable is "undefined"

		// let myVariable;

	let myVariable = "Hello";

	console.log(myVariable);

	/*
		Guides in writing variables
			- using the right keyword is a way for a dev to successfully initialize a variable
			- variable names should start with a lowercase character, and use camelCasing for multiple words
			- variable names should be indicative or descriptive of the value being stored to avoid confusion;

	*/

	/*Samples of Variable Guides

	let firstName = "Michael";
	let pokemon = 25000; 
		// bad naming for pokemon as it does not fully explain the value it is assigned with.

	let firstName = "Michael"; 
	let FirstName = "Michael";
		// use 1st variable = camelCasing

	let first name = "Michael";
	console.log(first name)
		// variables should not have spaces

	*/

	// Declaring and Initializing Variables

	let productName = "Desktop Computer"
	console.log(productName)

	// creata a productPrice variable with the value 18999

	let productPrice = 18999;
	console.log(productPrice)

	// in the context of certain applications, some variables/information are constant and should not be changed.
	// one example of this in real-world scenario is the interest for loan, savings account, or mortgage interest must not be changed due to implications in computation.

	const interest = 3.539;
	console.log(interest);


	// Reassigning of variable values
	// 		- means that we are going to change the initial/previous value into another value;

	productName = "Laptop";
	console.log(productName)

	// we change the value of a let variable

	/*
		SYNTAX:
			let variableName = initialValue;

			variableName = newValue;

	*/

	/*
		MINIACTIVITY
			create a "friend" variable and assign a name to it (log to console)
			then reassign a new name (log to console)
	*/

	let friend = "Davion"
	console.log(friend)

	friend = "Mirana"
	console.log(friend)


	// const variable values cannot and should not be changed by the devs
	// if we declare a variable using const, we can neither nor reassign the value.
	/*
	interest = 4.489;
	console.log(interest);
	*/

	/*
		when to use JS Const
			as a general rule, always declare a variable with const unless you know that the value will change.
	*/


	let supplier;
	supplier = "John Smith Tradings";
	console.log(supplier);


	// var is also used in declaring a variable. But var is an ECMAScript1 (ES1) feature [ES1 (Javascript 1997)]
	// let/const - introduced as new features of ES6 (2015)

	// difference

	// there are issues when it comes to varialbes declared using var, regarding hoisting
	// in terms of variables, keyword var is hoisted while let/const does no allow hoisting.
	// Hoisting is Javascript's default behavior of moving declarations to the top.

	// var vs let/const
	a = 5;
	console.log(a)
	var a;

	// scope of variables
	/*
		- scope essentially means where these variables are available for use
		- let and const variables are block scoped
		- a block is a chunk of codes bounded by {}

	*/

	let	outerVariable = "hello";
	{
		let innerVariable = "hello again";
		console.log(innerVariable);
	}

	console.log(outerVariable);
	// console.log(innerVariable);


	// multiple variable declarations
	/*
		multiple variables can be declared in one line using one keyword.
		the two variable declarations must be separated by a comma.
	*/
	let productCode = "CD017", productBrand = "Dell", productStore = "Makati";
	console.log(productCode);
	console.log(productBrand);
	console.log(productStore);


	// trying to use a varialbe with a reserved keyword
	/*
	const let = "hello";
	console.log(let)
	*/
	// since let is already reserved keyword in JS, it cannot be used as a variable name.



	// Datatypes in Javascript
	/*	
		STRING
			series of characters that create a word, phrase, sentence or anything related to create a text
	*/

	let country = "Philippines"
	console.log(country);

	// Concatenating strings
		// combining multiple values and variables with the "+" symbol
	let province = "Metro Manila";
	console.log(province + ", " + country);

	// escape character
	// "/n" would creat a new line break between the text

	let mailAddress = "Metro Manila\n\nPhilippines";
	console.log(mailAddress);

	// using double quotes and single quotes for string data types are actually valid in JS
	// if the string has a single quote/apostrophe, use "" for string indicator, so that we wont have to use ''
	console.log("John's Employess went home early.")
	console.log('John\'s Employess went home early.')

	/*
		INTEGER
	*/

	//Numbers
	// with the exception of strings, other datatypes in JS are COLOR-CODED
	let headCount = 26;
	console.log(headCount);

	// decimal/fractions
	let grade = 98.7;
	console.log(grade);


	// exonential notation
	let planetDistance = 2e10;
	console.log(planetDistance);
	

	// combining strings and numbers
	// when numbers and strings are combined, the resulting datatype would be a String
	console.log("John's grade last quarter is " + grade);

	let isMarried = false;
	let inGoodConduct = true;


	/*
		BOOLEAN
	*/ 
	// are normally used to store values relating to the state of certain things
	// true/false
	console.log("isMarried: " + isMarried);
	console.log("inGoodConduct: " + inGoodConduct);

	/*
		ARRAYS
	*/
	// arrays are a special kind of datatype that are used to store multiple values;
	// they can store different datatypes, but it is normally used to store "similar" datatypes;


	/*
		SYNTAX
			let/const varName = [elementA, elementB, elementC, ..., elementN];
	*/

	// similar data type
	let grades = [98.7, 92.1 , 90.2, 94.6];
	console.log(grades);

	// different data type
	// it is NOT advisable to use multiple data types in an array since it would be confusing for other devs when they read our codes.
	let person = ["John", "Smith", 32, true];
	console.log(person);


	/*
		OBJECTS
	*/
	//	objects are another special kind of data type that's used to mimic real world objects;
	// 	they are used to create complex data that contains pieces of information that are relevant to each other.


	/*
		SYNTAX
			let/const varName = {
				propertyA: value,
				propertyB: value,
				propertyC: [elementA, elementB, elementC]
			}
	*/

	let personDetails = {
		fullName: "Juan Dela Cruz",
		age: 35,
		isMarried: true,
		contactDetails: ["09123456789", "09987654321"],
		address:{
				houseNumber: "345",
				city: "Manila"
				},
	};
	console.log(personDetails);


	// typeof keyword - used if the dev are not sure or wants to confirm what the data type of a variable is
	console.log(typeof personDetails);


	/*
		Constant Array/Objects
			const keyword is misleading when it comes to arrays/objects. 

			it does not define a constant value of the elements inside the the array. it defines a constant reference to a value.

			we CANNOT reassign a constant value 
			we CANNOT reassign a constant array
			we CANNOT reassign a constant object

			but 
			we CAN change the elements of a const array
			we CAN change the properties of a const object

	*/

	const anime = ["Naruto", "Jujutsu Kaisen", "One Piece"];
	console.log(anime);
	/* would return an error because  we have const variable	
	anime = ["Akame ga Kill"]; 
	*/

	anime[0] = ["Akame ga Kill"];
	console.log(anime);



	/*
		NULL data type
	*/

	let number = 0;
	let string = "";
	console.log(number);
	console.log(string);


	/*
		NULL is used to explicitly express the absence of a value for a variable in a declaration/initialization

		one clear difference of null vs undefined is that null means that the varialbe was created and assigned a value that doe not hold any value

		compared to undefined which is concerned with creating a variable but was not given any value.
	*/

	let jowa = null;
	console.log(jowa);
